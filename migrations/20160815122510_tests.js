exports.up = function(knex, Promise) {
  return knex.schema.createTable('tests', function(table) {
    table.increments();
    table.string('test').notNullable().unique();
    table.timestamps();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('tests');
};
