import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './components/App';
import Greeting from './components/Greetings';
import SignUpPage from './components/signup/SignUpPage';
import LoginPage from './components/login/LoginPage';
import NewEventPage from './components/events/NewEventPage';

import requireAuth from './utils/requireAuth'

export default (
  <Route path="/" component={App} >
    <IndexRoute component={Greeting} />
    <Route path="signup" component={SignUpPage} />
    <Route path="login" component={LoginPage} />
    <Route path="new-event" component={requireAuth(NewEventPage)} />
  </Route>
)
