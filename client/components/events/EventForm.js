import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {createEvent} from '../../actions/eventActions';
import TextFieldGroup from '../common/TextFieldGroup';

class EventForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      errors: {},
      isLoading: false
    }
    
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  
  onChange(e) {
    this.setState({[e.target.name]: e.target.value});
  }
  
  isValid() {
    const {errors, isValid} = validateInput(this.state);
    
    if(!isValid) {
      this.setState({errors});
    }
    
    return isValid;
  }
  
  onSubmit(e) {
    e.preventDefault();
    this.props.createEvent(this.state);
    // if (this.isValid()) {
    //   this.setState({errors: '', isLoading: true})
    //   this.props.login(this.state).then(
    //     (res) => this.context.router.push('/'),
    //     (error) => {this.setState({errors: error.response.data.errors, isLoading: false})}
    //   );
    // }
  }
  
  render() {
    const { title, errors, isLoading } = this.state;
    return (
      <form onSubmit={this.onSubmit}>
        <h1>Create New Game Event</h1>
        
        {errors.form && <div className="alert alert-danger">{errors.form}</div>}
        
        <TextFieldGroup
          field="title"
          label="Title"
          value={title}
          error={errors.title}
          onChange={this.onChange} />
        
        <div className="form-group">
          <button disabled={isLoading} className="btn btn-primary btn-lg">
            {isLoading ? 'Loading...' : 'Login'}
          </button>
        </div>
      </form>
    );
  }
}

EventForm.propTypes = {
  createEvent: PropTypes.func.isRequired
};

export default connect(null, {createEvent})(EventForm);
