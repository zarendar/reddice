import React, {PropTypes} from 'react';
import EventForm from './EventForm';

class NewEventPage extends React.Component {
  render() {
    return (
      <EventForm />
    );
  }
}

NewEventPage.propTypes = {
  
};

export default NewEventPage;
