import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {deleteFlashMessage} from '../../actions/flashMessages';
import FlashMessage from './FlashMessage';

class FlashMessagesList extends React.Component {
  render() {
    const {messages, deleteFlashMessage} = this.props;
    const flashMessages = messages.map(message =>
      <FlashMessage 
        key={message.id} 
        message={message}
        deleteFlashMessage={deleteFlashMessage} />
    );
    
    return (
      <div>
        {flashMessages}
      </div>
    );
  }
}

FlashMessagesList.propTypes = {
  messages: PropTypes.array.isRequired
};

function mapStateToProps(state) {
  return {
    messages: state.flashMessages,
    deleteFlashMessage: PropTypes.func.isRequired
  }
}

export default connect(mapStateToProps, {deleteFlashMessage})(FlashMessagesList);
