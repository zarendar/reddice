import React, {PropTypes} from 'react';
import timezones from '../../data/timezones';
import map from 'lodash/map';
import classnames from 'classnames';
import validateInput from '../../../server/shared/validations/signup.js';
import TextFieldGroup from '../common/TextFieldGroup';

export default class SignUpForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      passwordConfirmation: '',
      timezone: '',
      errors: {},
      isLoading: false,
      invalid: false
    }
    
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.checkUserExist = this.checkUserExist.bind(this);
  }
  
  onChange(e) {
    this.setState({[e.target.name]: e.target.value});
  }
  
  isValid() {
    const {errors, isValid} = validateInput(this.state);
    
    if(!isValid) {
      this.setState({errors});
    }
    
    return isValid;
  }
  
  checkUserExist(e) {
    const field = e.target.name;
    const val = e.target.value;
    if(val !== '') {
      this.props.isUserExist(val).then(res => {
        let errors = this.state.errors;
        let invalid;
        if (res.data.user) {
          errors[field] = `There is user with such ${field}`
          invalid = true
        } else {
          errors[field] = ''
          invalid = false
        }
        this.setState({ errors, invalid });
      });
    }
  }
  
  onSubmit(e) {
    e.preventDefault();
    
    if (this.isValid()) {
      this.setState({errors: {}, isLoading: true});
      this.props.userSignUpRequest(this.state).then(
        () => {
          this.setState({isLoading: false})
          this.props.addFlashMessage({
            type: 'success',
            text: 'You signed up successfully. Welcome!'
          })
          this.context.router.push('/');
          
        },
        (error) => {this.setState({errors: error.response.data, isLoading: false})}
      )
    }
  }
  
  render() {
    const {errors} = this.state;
    const options = map(timezones, (val, key) =>
      <option key={val} value={val}>{key}</option>
    );
    
    return (
      <form onSubmit={this.onSubmit} noValidate>
        <h1>Join our community</h1>
        
        <TextFieldGroup 
          label="Username"
          error={errors.username}
          value={this.state.username}
          field="username"
          checkUserExist={this.checkUserExist}
          onChange={this.onChange} />
        
        <TextFieldGroup 
          label="Email"
          error={errors.email}
          value={this.state.email}
          field="email"
          checkUserExist={this.checkUserExist}
          onChange={this.onChange} />
        
        <TextFieldGroup 
          label="Password"
          error={errors.password}
          value={this.state.password}
          type="password"
          field="password"
          checkUserExist={this.checkUserExist}
          onChange={this.onChange} />
        
        <TextFieldGroup 
          label="PasswordConfirmation"
          error={errors.passwordConfirmation}
          value={this.state.passwordConfirmation}
          type="passwordConfirmation"
          field="passwordConfirmation"
          onChange={this.onChange} />
        
        <div className={classnames('form-group', {'has-error': errors.timezone})}>
          <label className="control-label">PasswordConfirmation</label>
          <select 
            value={this.state.timezone}
            name="timezone"
            className="form-control"
            onChange={this.onChange} >
            <option value="" disabled>Choose Your Timezone</option>
            {options}
          </select>
          {errors.timezone && <span className="help-block">{errors.timezone}</span>}
        </div>
        
        <div className="form-group">
          <button disabled={this.state.isLoading || this.state.invalid} className="btn btn-primary btn-lg">
            {this.state.isLoading ? 'Loading...' : 'Sign Up'}
          </button>
        </div>
      </form>
    );
  }
}

SignUpForm.propTypes = {
  userSignUpRequest: PropTypes.func.isRequired,
  addFlashMessage: PropTypes.func.isRequired,
  isUserExist: PropTypes.func.isRequired
};

SignUpForm.contextTypes = {
  router: PropTypes.object.isRequired
};
