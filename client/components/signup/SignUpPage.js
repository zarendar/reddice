import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {userSignUpRequest, isUserExist} from '../../actions/signUpActions';
import {addFlashMessage} from '../../actions/flashMessages';
import SignUpForm from './SignUpForm';

class SignUpPage extends React.Component {
  render() {
    const {userSignUpRequest, addFlashMessage, isUserExist} = this.props;
    
    return (
      <div className="row">
        <div className="col-md-4 col-md-offset-4">
          <SignUpForm 
            userSignUpRequest={userSignUpRequest}
            addFlashMessage={addFlashMessage}
            isUserExist={isUserExist} />
        </div>
      </div>
    );
  }
}

SignUpPage.propTypes = {
  userSignUpRequest: PropTypes.func.isRequired,
  addFlashMessage: PropTypes.func.isRequired,
  isUserExist: PropTypes.func.isRequired
};

export default connect(
  null,
  {userSignUpRequest, addFlashMessage, isUserExist}
)(SignUpPage);
