import express from 'express';
import authenticate from '../middlewares/authenticate';
import Event from '../models/event';

let router = express.Router();

router.post('/', authenticate, (req, res) => {
  const { title } = req.body;
  
  Event.forge({ title }, {hasTimestamps: true}).save()
    .then(event => { res.status(201).json({ event }) })
    .catch(err => res.status(500).json({error: err}));
  // res.status(201).json({user: req.currentUser});
});

export default router;
